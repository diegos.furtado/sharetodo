import React, { Component, createContext } from "react";

export const AppContext = createContext();

export class AppProvider extends Component {
  state = {
    snackBarOpen: false,
    snackBarVariant: "success",
    snackBarMessage: "",
    bottomSheetOpen: false
  };

  toggleBottomSheet = () =>
    this.setState({
      bottomSheetOpen: !this.state.bottomSheetOpen
    });

  snackOpen = (variant, message) =>
    this.setState({
      snackBarOpen: true,
      snackBarVariant: variant,
      snackBarMessage: message
    });

  snackClose = () => this.setState({ snackBarOpen: false });

  render() {
    return (
      <AppContext.Provider
        value={{
          ...this.state,
          snackOpen: this.snackOpen,
          snackClose: this.snackClose,
          toggleBottomSheet: this.toggleBottomSheet
        }}
      >
        {this.props.children}
      </AppContext.Provider>
    );
  }
}
