import { createStore, applyMiddleware, compose } from "redux";
import reduxThunk from "redux-thunk";

import reduceConfig from "../configs/reducerConfig";

export default preloadedState =>
  createStore(
    reduceConfig,
    preloadedState,
    compose(applyMiddleware(reduxThunk))
  );
