import React, { Component } from "react";
import cx from "classnames";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import queryString from "query-string";

//Containers
import BottomDrawerContainer from "./BottomDrawerContainer";

//Context
import { AppContext } from "../context";

//Actions
import { save } from "../TodoAction";

//Components
import InputComponent from "../components/InputComponent";
import ShowListComponent from "../components/ShowListComponent";
import BottomAppBarComponent from "../components/BottomAppBarComponent";
import SnackbarComponent from "../components/SnackbarComponent";

//ComponentsUI
import { withStyles } from "@material-ui/core/styles";
import Fade from "@material-ui/core/Fade";
import Fab from "@material-ui/core/Fab";
import CircularProgress from "@material-ui/core/CircularProgress";
import FormControl from "@material-ui/core/FormControl";
import Input from "@material-ui/core/Input";
import InputLabel from "@material-ui/core/InputLabel";
import Card from "@material-ui/core/Card";
import CardMedia from "@material-ui/core/CardMedia";

//Icons
import CheckIcon from "@material-ui/icons/Check";

//Assets
import "../assets/App.css";

import AppContainerStyle from "../assets/AppContainerStyle";

const styles = AppContainerStyle;
const imgSRC =
  "https://i.pinimg.com/originals/dd/17/76/dd177688bb67d6116f024035abad5739.png";

class AppContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      list: {
        items: [],
        title: "",
        id: undefined
      },
      loading: false,
      complete: false,
      drawerIsOpen: false
    };
  }

  componentWillMount() {
    if (this.props.location.search) {
      try {
        const urlString = queryString.parse(this.props.location.search);

        const obj =
          typeof urlString.item === "object"
            ? urlString.item.map((u, k) => ({
                item: urlString.item[k],
                complete: JSON.parse(urlString.complete[k])
              }))
            : [
                {
                  item: urlString.item,
                  complete: JSON.parse(urlString.complete)
                }
              ];

        this.setState({
          list: { items: obj, title: urlString.title, id: urlString.id }
        });
      } catch (e) {
        this.props.history.push("/");
      }
    }
  }

  componentDidMount() {
    window.addEventListener("newSWContent", evt => {
      console.log("new version is available", evt.detail);
    });
  }

  showPrompt = () => window.deferredPrompt.prompt();

  add = evt => {
    evt.preventDefault();

    const { items } = this.state.list;

    items.push({
      item: evt.target["item"].value,
      complete: false
    });

    this.setState({ items }, () => {
      document.getElementsByName("item")[0].value = "";
    });
  };

  toggleStatus = name => {
    const list = this.state.list.items.map(item => {
      if (item.item === name) {
        item.complete = !item.complete;
      }

      return item;
    });

    this.setState({ list: { ...this.state.list, items: list } });
  };

  deleteItem = name => {
    const filteredList = this.state.list.items.filter(
      item => item.item !== name
    );

    this.setState({ list: { ...this.state.list, items: filteredList } });
  };

  save = (evt, snackOpen) => {
    evt.preventDefault();

    const { items, title, id } = this.state.list;

    this.setState({ loading: true }, () =>
      setTimeout(() => {
        const todo = { ...items, title: title, id: id };

        this.props.actions
          .save(todo)
          .then(() => {
            this.setState({ loading: false, complete: true }, () => {
              snackOpen("success", `Lista salva com sucesso!`);

              setTimeout(() => this.props.history.push("/"), 3000);
            });
          })
          .catch(e => {
            this.setState({ loading: false, complete: false }, () =>
              snackOpen("error", e.message)
            );
          });
      }, 2000)
    );
  };

  toggleDrawer = () =>
    this.setState(state => ({ drawerIsOpen: !state.drawerIsOpen }));

  render() {
    const { list, drawerIsOpen, loading, complete, unmount } = this.state;
    const { classes } = this.props;

    return (
      <AppContext.Consumer>
        {({ snackOpen }) => (
          <div className="App">
            <div className={classes.background}>
              <InputComponent
                handleAdd={this.add}
                editState={Boolean(list.id)}
              />
            </div>

            {!list.items.length ? (
              <Card
                elevation={12}
                className={classes.paper}
                onClick={this.showPrompt}
              >
                <CardMedia
                  className={classes.media}
                  image={imgSRC}
                  title="Imagem para comecar uma nova lista"
                />
              </Card>
            ) : (
              <Fade in={Boolean(list.items.length)}>
                <form
                  onSubmit={e => this.save(e, snackOpen)}
                  className={cx(
                    classes.container,
                    unmount && "animated bounceOutDown"
                  )}
                >
                  <FormControl fullWidth>
                    <InputLabel htmlFor="title">Titulo</InputLabel>

                    <Input
                      id="title"
                      required
                      name="title"
                      value={list.title}
                      onChange={e =>
                        this.setState({
                          list: { ...list, title: e.target.value }
                        })
                      }
                    />
                  </FormControl>

                  <ShowListComponent
                    list={list.items}
                    toggleStatus={this.toggleStatus}
                    deleteItem={this.deleteItem}
                  />

                  <Fab
                    aria-label="AddButton"
                    className={cx(
                      classes.fab,
                      !loading && { _blue: !complete },
                      {
                        _green: complete
                      }
                    )}
                    disabled={!list.items.length || loading}
                    type="submit"
                  >
                    <CheckIcon />
                  </Fab>

                  {loading && (
                    <CircularProgress
                      size={68}
                      className={classes.fabProgress}
                    />
                  )}
                </form>
              </Fade>
            )}

            {/*BottomMenu*/}
            <BottomAppBarComponent toggleDrawer={this.toggleDrawer} />

            {/*Hidden components*/}
            <BottomDrawerContainer
              isOpen={drawerIsOpen}
              toggleDrawer={this.toggleDrawer}
            />

            <SnackbarComponent />
          </div>
        )}
      </AppContext.Consumer>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({ save }, dispatch)
});

export default withStyles(styles)(
  connect(
    null,
    mapDispatchToProps
  )(AppContainer)
);
