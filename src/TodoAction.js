import { add, all, del } from "./TodoApi";

export const getAll = () => async dispatch => {
  try {
    const list = await all();

    dispatch({ type: "GET_ALL_TODO", todo: list });
  } catch (e) {
    console.log("e", e.message);
    return e.message;
  }
};

export const save = list => async dispatch => {
  try {
    await add(list);

    return dispatch({ type: "SAVE_TODO", todo: list });
  } catch (e) {
    throw new Error(e);
  }
};

export const remove = item => async dispatch => {
  try {
    await del(item.id);

    return dispatch({ type: "DELETE_TODO", key: item.title });
  } catch (e) {
    console.log("e", e.message);
    return e.message;
  }
};
