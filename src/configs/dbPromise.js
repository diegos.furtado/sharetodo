import { openDb } from "idb";

const dbPromise = openDb("toDo", 1, upgradeDB => {
  if (!upgradeDB.objectStoreNames.contains("list")) {
    let listOS = upgradeDB.createObjectStore("list", {
      keyPath: "id",
      autoIncrement: true
    });

    listOS.createIndex("list_id", "id", { unique: true });
    listOS.createIndex("list_title", "title", { unique: true });
    listOS.createIndex("list_item", "item", { unique: true });
    listOS.createIndex("list_complete", "complete", { unique: false });
  }
}).then(db => {
  console.log("dbPromise create");
  return db;
});

export default dbPromise;
