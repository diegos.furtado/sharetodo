import green from "@material-ui/core/colors/green";

export default theme => ({
	background: {
		background: "linear-gradient(to right, #3498db, #2c3e50)",
		display: "inline-block",
		left: 0,
		top: 0,
		width: "100%",
		height: "180px",
		clipPath:
			"polygon(0% 0%, 100% 0%, 100% 75%, 100% 44%, 0% 83%, 56% 64%, 0% 83%)"
	},
	fab: {
		margin: theme.spacing.unit,
		color: "white",
		bottom: "60px",
		position: "fixed",
		right: "10px"
	},
	container: {
		margin: "0 20px"
	},
	fabProgress: {
		color: green[500],
		position: "fixed",
		bottom: "62px",
		right: "12px",
		zIndex: 1
	},
	paper: {
		margin: "0 20px",
		borderRadius: "20px"
	},
	media: {
		height: 220
	}
})