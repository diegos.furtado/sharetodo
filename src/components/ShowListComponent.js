import React from "react";
import { TransitionGroup, CSSTransition } from "react-transition-group";
import PropTypes from "prop-types";

//ComponentsUI
import { withStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import Checkbox from "@material-ui/core/Checkbox";
import IconButton from "@material-ui/core/IconButton";

//Icons
import DeleteOutlineIcon from "@material-ui/icons/DeleteOutline";

//Assets
import "../assets/fadeTransition.css";

const styles = {};

const ShowListComponent = ({ classes, list, toggleStatus, deleteItem }) => (
  <div className="showItems">
    <List className={classes.root}>
      <TransitionGroup>
        {list.map(item => (
          <CSSTransition timeout={500} classNames="fade" key={item.item}>
            <ListItem
              role={undefined}
              dense
              button
              onClick={() => toggleStatus(item.item)}
            >
              <Checkbox checked={item.complete} tabIndex={-1} disableRipple />
              <ListItemText primary={item.item} />
              <ListItemSecondaryAction>
                <IconButton
                  aria-label="Delete item"
                  onClick={() => deleteItem(item.item)}
                >
                  <DeleteOutlineIcon />
                </IconButton>
              </ListItemSecondaryAction>
            </ListItem>
          </CSSTransition>
        ))}
      </TransitionGroup>
    </List>
  </div>
);

ShowListComponent.propTypes = {
  classes: PropTypes.object.isRequired,
  list: PropTypes.array.isRequired,
  toggleStatus: PropTypes.func.isRequired,
  deleteItem: PropTypes.func.isRequired
};

export default withStyles(styles)(ShowListComponent);
