import React from "react";
import PropTypes from "prop-types";

//ComponentsUI
import { withStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";

//Icons
import BookmarkBorderIcon from "@material-ui/icons/BookmarkBorder";

const styles = {
  appBar: {
    top: "auto",
    bottom: 0,
    background: "linear-gradient(to right, #3498db, #2c3e50)"
  },
  toolbar: {
    alignItems: "center",
    justifyContent: "space-between"
  }
};

const BottomAppBarComponent = ({ classes, toggleDrawer }) => (
  <AppBar position="fixed" className={classes.appBar}>
    <Toolbar className={classes.toolbar}>
      <Typography component="h2" color="inherit" variant="title">
        ShareTodo
      </Typography>

      <div>
        <IconButton color="inherit" onClick={toggleDrawer}>
          <BookmarkBorderIcon />
        </IconButton>
      </div>
    </Toolbar>
  </AppBar>
);

BottomAppBarComponent.propTypes = {
  toggleDrawer: PropTypes.func.isRequired
};

export default withStyles(styles)(BottomAppBarComponent);
