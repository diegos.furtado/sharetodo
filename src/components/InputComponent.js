import React from "react";
import { Link } from "react-router-dom";

//ComponentsUI
import { withStyles } from "@material-ui/core/styles";
import InputBase from "@material-ui/core/InputBase";
import IconButton from "@material-ui/core/IconButton";
import Paper from "@material-ui/core/Paper";
import Divider from "@material-ui/core/Divider";

import PlaylistAddIcon from "@material-ui/icons/PlaylistAdd";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";

const styles = {
  root: {
    padding: "2px 4px",
    margin: "20px",
    display: "flex",
    alignItems: "center"
  },
  divider: {
    width: 1,
    height: 28,
    margin: 4
  },
  iconButton: {
    padding: 10
  },
  input: {
    marginLeft: 8,
    flex: 1
  }
};

const AppBar = ({ classes, handleAdd, editState }) => (
  <form onSubmit={e => handleAdd(e)}>
    <Paper className={classes.root} elevation={1}>
      {editState && (
        <Link to="/">
          <IconButton
            color="primary"
            className={classes.iconButton}
            type="submit"
          >
            <ChevronLeftIcon />
          </IconButton>
        </Link>
      )}

      <InputBase
        className={classes.input}
        name="item"
        required
        placeholder="Fazer compras"
      />

      <Divider className={classes.divider} />

      <IconButton color="primary" className={classes.iconButton} type="submit">
        <PlaylistAddIcon />
      </IconButton>
    </Paper>
  </form>
);

export default withStyles(styles)(AppBar);
