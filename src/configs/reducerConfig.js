import { combineReducers } from "redux";

import TodoReducer from "../TodoReducer";

const reducerConfig = combineReducers({
  todo: TodoReducer,
});


export default reducerConfig;
