import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import { TransitionGroup, CSSTransition } from "react-transition-group";

//Context
import { AppProvider } from "./context";

//Components
import AppContainer from "./containers/AppContainer";

//Assets
import "./assets/index.css";
import "./assets/fadeInUpTransition.css";

//serviceWorker
import * as serviceWorker from "./serviceWorker";

//Configs
import storeConfig from "./configs/storeConfig";
import "./configs/dbPromise";

const store = storeConfig();

ReactDOM.render(
  <Provider store={store}>
    <AppProvider>
      <BrowserRouter>
        <Route
          render={({ location }) => (
            <TransitionGroup>
              <CSSTransition
                key={location.key}
                classNames="fadeInUp"
                timeout={500}
              >
                <Switch location={location}>
                  <Route path="/" component={AppContainer} />
                </Switch>
              </CSSTransition>
            </TransitionGroup>
          )}
        />
      </BrowserRouter>
    </AppProvider>
  </Provider>,
  document.getElementById("root")
);

serviceWorker.register();
