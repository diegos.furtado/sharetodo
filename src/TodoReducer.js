export default (state = [], action) => {
  switch (action.type) {
    case "GET_ALL_TODO":
      return action.todo;
    case "SAVE_TODO":
          return [...state, Object.assign({}, action.todo)]
    case "DELETE_TODO":
      return state.filter(todo => todo.title !== action.key);
    default:
      return state;
  }
};
