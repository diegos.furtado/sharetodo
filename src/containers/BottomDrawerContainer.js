import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { bindActionCreators } from "redux";
import PropTypes from "prop-types";
import queryString from "query-string";

//Actions
import { getAll, remove } from "../TodoAction";

//Components
import Drawer from "@material-ui/core/Drawer";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import Collapse from "@material-ui/core/Collapse";
import ListItemText from "@material-ui/core/ListItemText";

//Icons
import BookmarkIcon from "@material-ui/icons/Bookmark";
import DeleteOutlineIcon from "@material-ui/icons/DeleteOutline";
import EditIcon from "@material-ui/icons/Edit";
import ShareIcon from "@material-ui/icons/Share";
import ExpandLessIcon from "@material-ui/icons/ExpandLess";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";

class BottomDrawerContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      open: undefined,
      actions: [
        { title: "Compartilhar", icon: <ShareIcon />, action: this.shareList },
        { title: "Editar", icon: <EditIcon />, action: this.findList },
        {
          title: "Deletar",
          icon: <DeleteOutlineIcon />,
          action: props.actions.remove
        }
      ]
    };
  }

  componentDidMount() {
    this.props.actions.getAll();
  }

  makeQueryParams = data =>
    Object.keys(data)
      .map(r =>
        typeof data[r] === "object"
          ? queryString.stringify(data[r])
          : encodeURIComponent(r) + "=" + encodeURIComponent(data[r])
      )
      .join("&");

  findList = item => this.props.history.push(`/?${this.makeQueryParams(item)}`);

  toggleItem = item => this.setState({ open: item.title });

  shareList = item => {
    if (navigator.share) {
      navigator
        .share({
          title: "Lista do todoShare",
          text: `Da uma olhada nessa minha lista — ${item.title}`,
          url: `https://sharetodo-6e13d.firebaseapp.com/?${this.makeQueryParams(
            item
          )}`
        })
        .then(() => console.log("Successful share"))
        .catch(error => console.log("Error sharing", error));
    }
  };

  render() {
    const { isOpen, toggleDrawer, list } = this.props;

    return (
      <Drawer
        anchor="bottom"
        open={isOpen}
        onClose={() => toggleDrawer("bottom", false)}
      >
        <div tabIndex={0} role="button">
          <List component="nav">
            {list.map(item => (
              <div key={item.title}>
                <ListItem button onClick={() => this.toggleItem(item)}>
                  <ListItemIcon>
                    <BookmarkIcon />
                  </ListItemIcon>

                  <ListItemText primary={item.title} />

                  {this.state.open === item.title ? (
                    <ExpandLessIcon />
                  ) : (
                    <ExpandMoreIcon />
                  )}
                </ListItem>
                <Collapse
                  in={this.state.open === item.title}
                  timeout="auto"
                  unmountOnExit
                >
                  <List component="div">
                    {this.state.actions.map(a => (
                      <ListItem
                        button
                        key={a.title}
                        onClick={() => a.action(item)}
                      >
                        <ListItemText inset primary={a.title} />

                        <ListItemIcon>{a.icon}</ListItemIcon>
                      </ListItem>
                    ))}
                  </List>
                </Collapse>
              </div>
            ))}
          </List>
        </div>
      </Drawer>
    );
  }
}

BottomDrawerContainer.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  toggleDrawer: PropTypes.func.isRequired,
  list: PropTypes.array.isRequired
};

const mapStateToProps = state => ({
  list: state.todo
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({ getAll, remove }, dispatch)
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(BottomDrawerContainer));
