import dbPromise from "./configs/dbPromise";
import uuid from "uuid";

export const all = () =>
  dbPromise
    .then(db => {
      const tx = db.transaction("list");

      return tx.objectStore("list").getAll();
    })
    .then(res => res);

export const find = title =>
  dbPromise
    .then(db => {
      const tx = db.transaction("list");
      const store = tx.objectStore("list");
      const index = store.index("list_title");

      return index.getAll(title);
    })
    .then(res => res);

export const add = list =>
  new Promise((resolve, reject) =>
    dbPromise
      .then(db => {
        const tx = db.transaction("list", "readwrite");

        tx.objectStore("list").put({ ...list, id: list.id || uuid() });

        return tx.complete;
      })
      .then(res => resolve(res))
      .catch(() => reject("Titulo já existe"))
  );

export const del = id =>
  dbPromise
    .then(db => {
      const tx = db.transaction("list", "readwrite");

      tx.objectStore("list").delete(id);

      return tx.complete;
    })
    .then(res => res);
